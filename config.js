module.exports = {
  db: {
    url: 'mongodb://localhost:27017',
    name: 'chat'
  },
  jwt: {
    secret: 'some kinda very secret string',
    expires: 3600000
  }
};