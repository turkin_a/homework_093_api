const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropDatabase();
  } catch (e) {
    console.log('Database were not present, skipping drop...');
  }

  await User.create({
    username: 'user',
    password: 'user',
    role: 'user'
  }, {
    username: 'admin',
    password: 'admin',
    role: 'moderator'
  });

  db.close();
});